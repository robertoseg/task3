output "name_server" {
  value = aws_route53_zone.domain.name_servers
}

output "public_ip" {
  value = [aws_eip.eip.public_ip]
}