provider "aws" {
  region = var.region
}
resource "aws_instance" "webserver_instance" {
  ami           = var.ami_id
  availability_zone       = var.av_zona
  instance_type = var.instance_type
  subnet_id     = aws_subnet.subnet_public.id

  # Security group assign to instance
  vpc_security_group_ids = [aws_security_group.SG_webserver.id]
  private_ip             = "10.0.1.10"
  # key name
  key_name = var.key_name

  tags = {
    Name = var.tags
  }
}

resource "aws_eip" "eip" {
  instance = aws_instance.webserver_instance.id
  vpc      = true
}