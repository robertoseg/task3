variable "region" {
  description = "The name of the selected region."
  type        = string
  default     = "us-east-1"
}

variable "ami_id" {
  description = "AMI to use for the instance"
  type        = string
  default     = "you AMI"
}

variable "instance_type" {
  description = "Type of instance to start"
  type        = string
  default     = "t3.micro"
}

variable "device_name" {
  description = "Name of the device to mount"
  type    = string
  default = "/dev/xvdh"
}

variable "key_name" {
  type    = string
  default = "you key name"
}

variable "cidr" {
  description = "The CIDR block for the VPC. Default value is a valid CIDR, but not acceptable by AWS and should be overridden"
  type        = string
  default     = "10.0.0.0/16"
}
variable "instance_tenancy" {
  description = "A tenancy option for instances launched into the VPC"
  type        = string
  default     = "default"
}

variable "enable_dns_hostnames" {
  description = "Should be true to enable DNS hostnames in the VPC"
  type        = bool
  default     = true
}

variable "enable_dns_support" {
  description = "Should be true to enable DNS support in the VPC"
  type        = bool
  default     = true
}

variable "enable_classiclink" {
  description = "Should be true to enable ClassicLink for the VPC. Only valid in regions and accounts that support EC2 Classic."
  type        = bool
  default     = false
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = string
  default     = "your tag"
}

variable "av_zona" {
  description = " availability zone all resources"
  type        = string
  default     = "us-east-1a"
}