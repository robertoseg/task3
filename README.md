<h1>Packer Ansible terraform</h1>

using packer to create an AMI in aws with the playbook created in task 2, use terraform to create the infrastructure (DNS, Instancias, SG, VPC) using the AMI created with packer.


```
├── Packer
│   ├── build_artifact.txt
│   ├── main.json
│   └── provisioners
│       ├── host
│       ├── playbook.yml
│       ├── README.md
│       └── roles
├── README.md
└── Terraform
    ├── instance.tf
    ├── route53.tf
    ├── variables.tf
    └── vpc.tf

4 directories, 10 files
```
